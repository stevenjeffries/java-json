Java JSON Library
=================

My Java JSON library aims at being simple to use, but robust enough for 
any use case.

Key Features
------------

- Parse JSON from a String, File, or Stream.
  - Get specific information on a parsing error.
- Quickly and easily build an object.
- Quickly and easily modify an object.

What's Next?
------------

- Formatting output
- Easily save an object

Installation
------------

Just download the json.jar file and include it in your path. No other
libraries are required.

It does require Java 8 though as lambda expressions are used heavily.

Usage
=====

Getting up and running with my JSON library should be really easy. There
are 3 different classes that you'll be working with the most:

**Json:** This is the class that is used to parse JSON from a file,
string, or stream.

**JsonValue:** This is any value that can be stored and/or loaded in a 
JSON string. The different types it can hold are string, int, double,
boolean, object, array, and undefined.

**JsonObject:** This is a JSON object filled with key/value pairs. The
keys are always strings, and the values are always JsonValues (each 
JsonValue can also be a JsonObject).

Creating a New JSON Object
--------------------------

Creating a JSON object from scratch is super easy.
~~~java
// Just import this one class.
import sej.json.JsonObject;

/*...*/
    // Then, you can construct a simple JSON object.
    JsonObject obj = new JsonObject()
        .set("int_val", 4)
        .set("double_val", Math.PI)
        .set("string_val", "some string")
        .set("boolean_val", true)
        .set("array_val", 1, 2, "three", true)
        .set("object_val", new JsonObject()
            .set("inner_1", 1)
            .set("inner_2", 2)
        );
    
    // And loop through each value it has.
    obj.each((key, val) -> {
        System.out.println("[" + key + "] is a " + val.getType()
            + " that = " + val);
    });

/*...*/
~~~

The above will output:
~~~text
[string_val] is a STRING that = "some string"
[int_val] is a INT that = 4
[double_val] is a DOUBLE that = 3.141592653589793
[object_val] is a OBJECT that = {"inner_2":2,"inner_1":1}
[boolean_val] is a BOOLEAN that = true
[array_val] is a ARRAY that = [1,2,"three",true]
~~~

Parsing a JSON String
---------------------

The Json class has many methods of parsing JSON from a string, stream, 
or file and in different encodings.

Parsing some simple strings:
~~~java
// Import this class to parse strings.
import sej.json.Json;

// Import this class for the parsed value.
import sej.json.JsonValue;
// Import this class for objects.
import sej.json.JsonObject;

/*... throws JsonParseException */
    // Now, we can just call the parse method 
    JsonValue value = Json.parse("4");
    System.out.println(value.getType() + ": " + value); // INT: 4
    
    value = Json.parse("\"str\"");
    System.out.println(value.getType() + ": " + value); // STRING: "str"
      
    value = Json.parse("[1, 2, 3, 4]");
    System.out.println(value.getType() + ": " + value); // ARRAY: [1,2,3,4]
    
    JsonObject obj = Json.parse("{'a':'some val', 'b': 4}").asObject();
    value = obj.get("a");
    System.out.println(value.getType() + ": " + value); // STRING: "a"
    value = obj.get("b");
    System.out.println(value.getType() + ": " + value); // INT: 4
    value = obj.get("c");
    System.out.println(value.getType() + ": " + value); // UNDEFINED: UNDEFINED
    
/*...*/
~~~

The Json class' parse method can also take files and streams as 
parameters, and each has an optional second parameter to indicate the 
encoding.

Parsing a file:
~~~java
import sej.json.Json;
import sej.json.JsonObject;

/*... throws JsonParseException */
    // Load by a filename.
    JsonObject obj = Json.load("path/to/file").asObject();
    
    // Load by file.
    File f = /* get some file */;
    obj = Json.load(f).asObject();
    
    // From a stream:
    InputStream stream = /* web resource, file resource, etc. */
    obj = Json.load(stream).asObject();
    
    // Or, if your stream has a different encoding:
    obj = Json.load(stream, "UTF-8").asObject();
/*...*/
~~~

To make this easy for developers, I've also included some detailed 
parsing exceptions.

The JsonParseException:
~~~
import sej.json.Json;
import sej.json.JsonParseException;

/*...*/        
    // Some bad JSON
    String json = "{'a':'some string','b':some other string','c':'good again'}";

    // Try to load it.
    try {
        Json.parse(json);
    } catch (JsonParseException ex) {
        System.out.println("Parse error:");
        System.out.println(ex.getMessage());
    }
/*...*/
~~~

The exception will give the line and column number in the JSON and it 
will even show exactly where the error occurs.

The output:
~~~text
Parse error:
Unexpected character 's'.

[1:24] ...(:'some string','b':some other string','c)...
                              ⥣

~~~

Modifying an Object
-------------------

~~~java
import sej.json.JsonObject;
/*...*/  
    // We'll use the same object from the earlier example.
    JsonObject obj = new JsonObject()
            .set("int_val", 4)
            .set("double_val", Math.PI)
            .set("string_val", "some string")
            .set("boolean_val", true)
            .set("array_val", 1, 2, "three", true)
            .set("object_val", new JsonObject()
                    .set("inner_1", 1)
                    .set("inner_2", 2)
            );

    // The basic set method is used to set a value.
    obj.set("int_val", 5);
    System.out.println(obj.get("int_val")); // 5

    // It can be used to set any value, and it will infer the type.
    obj.set("new_val", "I'm new");
    System.out.println(obj.get("new_val").getType()); // STRING

    // Let's say you want to perform some kind of operation on it:
    obj.setInt("int_val", (old_val) -> old_val * 2 + 3);
    System.out.println(obj.get("int_val")); // 13

    // Lambda expressions can be used for each type.
    obj.setString("string_val", (old_val) -> {
        old_val = old_val.toUpperCase();
        return old_val + ", and more";
    });
    System.out.println(obj.get("string_val")); // "SOME STRING, and more"

    // Method references can also be used in place of lambda expressions.
    obj.setString("string_val", String::toUpperCase);
    System.out.println(obj.get("string_val")); // "SOME STRING, AND MORE"

    // These mod methods become very useful when working with nested objects.
    obj.setObject("object_val", (obj_ref) -> {
        obj_ref
            .setInt("inner_1", (old_val) -> old_val + 3)
            .set("foo", "bar");
    });
    System.out.println(obj.get("object_val")); // {"foo":"bar","inner_2":2,"inner_1":4}

    // Fields can be removed as well.
    System.out.println(obj.getObject("object_val").get("inner_2")); // 2
    obj.getObject("object_val").removeField("inner_2");
    System.out.println(obj.getObject("object_val").get("inner_2")); // UNDEFINED
    
    // You can even keep only the fields you want:
    obj.removeAllFieldsExcept("int_val");
    System.out.println(obj); // {"int_val":13}

    // JsonObjects can be merged into other ones. This will actually change the
    // object and each of its values will stay in place, but any new values from
    // the other one will be added. Useful for settings and things.
    JsonObject defaults = new JsonObject()
            .set("thread_count", 3)
            .set("download_path", "/some/dir");

    JsonObject settings = new JsonObject()
            .set("download_path", "/foo/bar");

    settings.mergeWith(defaults);

    System.out.println(settings); // {"download_path":"/foo/bar","thread_count":3}

    // JsonObject has a copy method available if you want to perform the merge
    // without making any actual changes to the object.
    JsonObject userSettings = defaults.copy().mergeWith(new JsonObject().set("other_thing", "some value"));
/*...*/
~~~