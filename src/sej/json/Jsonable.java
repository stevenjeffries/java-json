package sej.json;


/**
 *
 * @author Steven Jeffries
 */
public interface Jsonable {
    public JsonObject toJson();
}
