package sej.json;

/**
 *
 * @author Steven Jeffries
 */
public enum ValueType {

    /**
     * A String data type.
     */
    STRING,
    /**
     * An int data type.
     */
    INT,
    /**
     * A double data type.
     */
    DOUBLE,
    /**
     * A JsonObject data type.
     */
    OBJECT,
    /**
     * A boolean data type.
     */
    BOOLEAN,
    /**
     * An array (or list) data type.
     */
    ARRAY,
    /**
     * Null data type. The only value stored if this type is given is null.
     */
    NULL,
    /**
     * Specifies that the field has not been set.
     */
    UNDEFINED
}
