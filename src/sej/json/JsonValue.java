package sej.json;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Steven Jeffries
 */
public final class JsonValue {

    public static JsonValue undefined() {
        JsonValue v = new JsonValue();
        v.value = ValueType.UNDEFINED;
        v.type = ValueType.UNDEFINED;
        return v;
    }

    /**
     * An interface meant to be used as a lambda expression. Used for looping through each JsonValue.
     */
    public static interface ValueLooper {

        public void loop(JsonValue val);
    }

    /**
     * An interface meant to be used as a lambda expression. Used to modify the value as an integer given the previous
     * value.
     */
    public static interface IntMod {

        public int mod(int i);
    }

    /**
     * An interface meant to be used as a lambda expression. Used to modify the value as a double given the previous
     * value.
     */
    public static interface DoubleMod {

        public double mod(double d);
    }

    /**
     * An interface meant to be used as a lambda expression. Used to modify the value as a boolean given the previous
     * value.
     */
    public static interface BooleanMod {

        public boolean mod(boolean b);
    }

    /**
     * An interface meant to be used as a lambda expression. Used to modify the value as a String given the previous
     * value.
     */
    public static interface StringMod {

        public String mod(String s);
    }

    /**
     * An interface meant to be used as a lambda expression. Used to modify the value as a JsonObject given the previous
     * value.
     */
    public static interface ObjectMod {

        public void mod(JsonObject obj);
    }

    /**
     * An interface meant to be used as a lambda expression. Used to modify the value as an array given the previous
     * value.
     */
    public static interface ArrayMod {

        public JsonValue[] mod(JsonValue[] vals);
    }

    /**
     * An interface meant to be used as a lambda expression. Used to modify the value as a list given the previous
     * value. Note: The type of this value is still array even though a list is given for convenience.
     */
    public static interface ListMod {

        public void mod(List<JsonValue> vals);
    }

    /**
     * The owner object that contains this value.
     */
    JsonObject owner;

    /**
     * The value of this object.
     */
    private Object value;

    /**
     * The type of data that value contains.
     */
    private ValueType type = ValueType.STRING;

    /**
     * Used internally for converting to a list. Since converting to a list is an expensive operation, this boolean is
     * used as a flag to see if it is safe to just explicitly cast the value into a linked list, or if this value is
     * false, then the list must be constructed.
     */
    private boolean isSafe = false;

    /**
     * Creates a new empty json value. By default, it will be a null data type and have a value of null.
     */
    public JsonValue() {
        set((Object) null);
    }

    /**
     * Creates a new json value given a value, or a list of values. If a list of values are given, then this data type
     * will be an array, and each item in the array will itself be a JsonValue.
     *
     * @param value The value(s) to set this to.
     */
    public JsonValue(Object... value) {
        set(value);
    }

    /**
     * Creates a new JsonValue that is a copy of another JsonValue. This is not a deep copy, only the value is set from
     * the other object. To create a deep copy, use {@code copy()}.
     *
     * @param toCopy The JsonValue to copy.
     */
    public JsonValue(JsonValue toCopy) {
        set(toCopy.value);
    }

    /**
     * Explicitly sets a type to the value. This will attempt the best it can to convert the value to its appropriate
     * type, but a default value may be used instead if it can't. However, the type will be reflected no matter what the
     * value given is (unless the type is null, or {@code ValueType.UNDEFINED}, then it is set to {@code ValueType.NULL}
     * instead.
     *
     * @param type  The type to set this object to.
     * @param value The value to set this object to.
     */
    public JsonValue(ValueType type, Object value) {
        if (type == null) {
            type = ValueType.NULL;
        }
        // Force "as" methods to try to parse value.
        this.type = ValueType.UNDEFINED;
        if (type == ValueType.ARRAY) {
            if (value == null) {
                setArray(new Object[]{});
            } else if (!value.getClass().isArray()) {
                setArray(new Object[]{value});
            } else {
                set(value);
            }
        } else if (type == ValueType.BOOLEAN) {
            this.value = value;
            asBoolean();
        } else if (type == ValueType.DOUBLE) {
            this.value = value;
            asDouble();
        } else if (type == ValueType.INT) {
            this.value = value;
            asInt();
        } else if (type == ValueType.OBJECT) {
            this.value = value;
            asObject();
        } else if (type == ValueType.STRING) {
            this.value = value;
            asString();
        } else if (type == ValueType.UNDEFINED) {
            this.value = ValueType.UNDEFINED;
        } else {
            this.value = null;
        }
        this.type = type;
    }

    public JsonValue copy() {
        try {
            return Json.parse(toString());
        } catch (JsonParseException ex) {
            // Should never happen.
            return null;
        }
    }

    /**
     * Used internally for converting an Object into an Object[]. Note: it is not safe to call this method with null, or
     * an object that is not an array.
     *
     * @param array The Object that represents an array.
     * @return An array.
     */
    private Object[] toArray(Object array) {
        LinkedList list = new LinkedList();
        int length = Array.getLength(array);
        for (int i = 0; i < length; i++) {
            list.add(Array.get(array, i));
        }
        return list.toArray();
    }

    /**
     * Used internally to check if the value is actually a list of JsonValue objects.
     *
     * @param obj The object to check.
     * @return True if it is a list of JsonValue objects, false otherwise.
     */
    private boolean isJsonList(Object obj) {
        if (obj == null || !(obj instanceof LinkedList)) {
            return false;
        }
        LinkedList ll = (LinkedList) obj;
        for (Object o : ll) {
            if (o == null || !(o instanceof JsonValue)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Used internally to create a copy of the linked list that holds the values of this (as an array).
     *
     * @return A copy of values.
     */
    private LinkedList<JsonValue> valuesCpy() {
        return new LinkedList<>(values());
    }

    /**
     * Gets the linked list representing the array contents (assuming this object is an array), or an empty linked list
     * if this object is not an array.
     *
     * @return The current value as a linked list.
     */
    private LinkedList<JsonValue> values() {
        if (!isSafe && !isJsonList(value)) {
            value = new LinkedList<>();
            isSafe = true;
            type = ValueType.ARRAY;
        }
        return (LinkedList<JsonValue>) value;
    }

    /**
     * Gets what type of value this object holds.
     *
     * @return
     */
    public ValueType getType() {
        return type;
    }

    /**
     * Sets this object's value. If more than one value, or an array is given, then this object becomes of the type
     * ValueType.ARRAY, and each subsequent item in the array is made into a JsonValue object.
     *
     * @param value The value(s) to set this object's value to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject set(Object... value) {
        this.value = null;
        isSafe = false;
        if (value == null || value.length == 0) {
            type = ValueType.NULL;
        } else if (value.length > 1) {
            type = ValueType.ARRAY;
            for (Object o : value) {
                values().add(new JsonValue(o));
            }
        } else {
            Object o = value[0];
            if (o == ValueType.UNDEFINED) {
                type = ValueType.UNDEFINED;
                this.value = type;
            } else if (o == null) {
                type = ValueType.NULL;
            } else if (o instanceof JsonValue) {
                return set(((JsonValue) o).value);
            } else if (o.getClass().isArray()) {
                return set(toArray(value));
            } else if (o instanceof Boolean) {
                type = ValueType.BOOLEAN;
                this.value = o;
            } else if (o instanceof Number) {
                if (o instanceof Double || o instanceof Float) {
                    type = ValueType.DOUBLE;
                } else {
                    type = ValueType.INT;
                }
                this.value = o;
            } else if (o instanceof JsonObject) {
                type = ValueType.OBJECT;
                this.value = o;
            } else if (isJsonList(o)) {
                type = ValueType.ARRAY;
                this.value = o;
            } else {
                type = ValueType.STRING;
                this.value = String.valueOf(o);
            }
        }
        return owner;
    }

    /**
     * Sets this value to a boolean, given the old value as a boolean.
     * <p>
     * An example call is {@code val.setBoolean((boolean b) -> !b);}
     *
     * @param mod A lambda expression (or interface instance) to set the value to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setBoolean(BooleanMod mod) {
        value = mod.mod(asBoolean());
        return owner;
    }

    /**
     * Sets this value to a boolean.
     *
     * @param val The boolean value to set this value as.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setBoolean(boolean val) {
        return set(val);
    }

    /**
     * Sets this value to a String, given the old value as a String.
     * <p>
     * An example call is {@code val.setString((String s) -> s.trim());}
     *
     * @param mod A lambda expression to set the value to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setString(StringMod mod) {
        type = ValueType.STRING;
        value = mod.mod(asString());
        return owner;
    }

    /**
     * Sets this value to a String.
     *
     * @param val The string to set this value to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setString(String val) {
        return set(val);
    }

    /**
     * Sets this value to an int, given the old value as an int.
     * <p>
     * An example call is {@code val.setInt((int i) -> i * 2);}
     *
     * @param mod A lambda expression to set the value to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setInt(IntMod mod) {
        value = mod.mod(asInt());
        return owner;
    }

    /**
     * Sets this value to an int.
     *
     * @param val The value to set.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setInt(int val) {
        return set(val);
    }

    /**
     * Sets this value to a double, given the old value as a double.
     * <p>
     * An example call is {@code val.setDouble((double d) -> Math.PI * d);}
     *
     * @param mod A lambda expression to set the value to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setDouble(DoubleMod mod) {
        value = mod.mod(asDouble());
        return owner;
    }

    /**
     * Sets this value to a double.
     *
     * @param val The value to set.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setDouble(double val) {
        return set(val);
    }

    /**
     * Sets this value to a JsonObject, given the old value as a JsonObject.
     * <p>
     * An example call is {@code val.setObject((JsonObject o) -> o.set("foo", "bar"));}
     *
     * @param mod A lambda expression to set the value to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setObject(ObjectMod mod) {
        mod.mod((JsonObject) (value = asObject()));
        return owner;
    }

    /**
     * Sets this value to a JsonObject.
     *
     * @param obj The JsonObject to set this value to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setObject(JsonObject obj) {
        return set(obj);
    }

    /**
     * Sets this value to an array, given the old value as an array.
     * <p>
     * An example call is {@code val.setArray((JsonValue[] arr) -> Arrays.copyOf(arr, arr.length + 1);}
     *
     * @param mod A lambda expression to set the value to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setArray(ArrayMod mod) {
        JsonValue[] arr = asArray();
        arr = mod.mod(arr);
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == null || !(arr[i] instanceof JsonValue)) {
                arr[i] = new JsonValue(arr[i]);
            }
        }
        value = new LinkedList<>(Arrays.asList(arr));
        return owner;
    }

    /**
     * Sets this value to an array.
     *
     * @param arr The array of JsonValue objects to set it to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setArray(JsonValue[] arr) {
        return set((Object[]) arr);
    }

    /**
     * Sets this value to an array.
     *
     * @param arr The array of values to set.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setArray(Object[] arr) {
        return set(arr);
    }

    /**
     * Sets this value to an array, given the old value as a list of JsonValue objects.
     * <p>
     * An example call is null null null null null null null null     {@code val.setArray((List<JsonValue> list) -> {
     * if (!list.isEmpty()) {
     * list.get(0).set("First element is this String");
     * }
     * list.add("Another String");
     * return list;
     * });}
     *
     * @param mod A lambda expression to set the value to.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setArray(ListMod mod) {
        mod.mod(asList());
        return owner;
    }

    /**
     * Sets this value to an array given a list of values to add.
     *
     * @param values The list of values to add.
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setArray(List<JsonValue> values) {
        if (values == null) {
            values = new LinkedList<>();
        } else if (!(values instanceof LinkedList)) {
            values = new LinkedList<>(values);
        }
        return set(values);
    }

    /**
     * Sets this value to null.
     *
     * @return A reference to the owning JsonObject object, or null if this object has no parent.
     */
    public JsonObject setNull() {
        value = null;
        type = ValueType.NULL;
        return owner;
    }

    /**
     * Loops through each object in this array, if this an array type. If this is not an array type, it becomes an empty
     * array type and the loop is never called.
     *
     * @param loop
     */
    public void each(ValueLooper loop) {
        for (JsonValue v : values()) {
            loop.loop(v);
        }
    }

    /**
     * Gets this value as a JsonObject if it already was one. If it was not, then the value is created as an empty
     * JsonObject, and that is returned.
     * <p>
     * Note: This value's type is set to {@code ValueType.OBJECT} after this call completes.
     *
     * @return This value as a JsonObject.
     */
    public JsonObject asObject() {
        if (type != ValueType.OBJECT) {
            value = new JsonObject();
            type = ValueType.OBJECT;
        }
        return (JsonObject) value;
    }

    /**
     * Gets this value as a double if it already was a number. If it was not, then a double is attempted to be parsed
     * from the value as a String. If that fails, then 1 or 0 is returned depending on how this value would be evaluated
     * as a boolean. See {@link asBoolean()}.
     * <p>
     * Note: This value's type is set to {@code ValueType.DOUBLE} after this call completes.
     *
     * @return This value as a double.
     */
    public double asDouble() {
        double ret;
        if (type == ValueType.DOUBLE || type == ValueType.INT) {
            ret = (double) value;
        } else {
            try {
                ret = Double.parseDouble(String.valueOf(value));
            } catch (Exception ex) {
                ret = asBoolean() ? 1 : 0;
            }
        }
        type = ValueType.DOUBLE;
        value = ret;
        return ret;
    }

    /**
     * Gets this value as an int if it already was a number. If it was not, then an int is attempted to be parsed from
     * the value as a String. If that fails, then 1 or 0 is returned depending on how this value would be evaluated as a
     * boolean. See {@link asBoolean()}.
     * <p>
     * Note: This value's type is set to {@code ValueType.INT} after this call completes.
     *
     * @return This value as an int.
     */
    public int asInt() {
        int ret;
        if (type == ValueType.DOUBLE || type == ValueType.INT) {
            ret = (int) value;
        } else {
            try {
                ret = Integer.parseInt(String.valueOf(value));
            } catch (Exception ex) {
                ret = asBoolean() ? 1 : 0;
            }
        }
        type = ValueType.INT;
        value = ret;
        return ret;
    }

    /**
     * Gets this value as a list of JsonValue objects if it already was an array. If it was not, then it is converted
     * into an empty array and given back as an empty list.
     * <p>
     * Note: This value's type is set to {@code ValueType.ARRAY} after this call completes.
     *
     * @return This value as a list.
     */
    public List<JsonValue> asList() {
        type = ValueType.ARRAY;
        return values();
    }

    /**
     * Gets this value as an array of JsonValue objects if it already was an array. If it was not, then it is converted
     * into an empty array and given back.
     * <p>
     * Note: This value's type is set to {@code ValueType.ARRAY} after this call completes.
     *
     * @return This value as an array.
     */
    public JsonValue[] asArray() {
        type = ValueType.ARRAY;
        return values().toArray(new JsonValue[values().size()]);
    }

    /**
     * Gets this value as a String if it already was one. If it was not, then {@code String.valueOf(value)} is returned.
     * <p>
     * Note: This value's type is set to {@code ValueType.STRING} after this call completes.
     *
     * @return This value as a String.
     */
    public String asString() {
        String s;
        if (type == ValueType.ARRAY) {
            s = toString();
        } else {
            s = String.valueOf(value);
        }
        type = ValueType.STRING;
        value = s;
        return s;
    }

    /**
     * Gets this value as a boolean if it already was one. If it was not, then it follows basic JavaScript rules for
     * determining whether true or false is returned. If it is a number, then true is returned unless the number is
     * equal to 0. If it is an object or array, then true is returned unless it is empty. If it is a String, then true
     * is returned unless it is an empty String. If the value is null, then false is returned.
     * <p>
     * Note: This value's type is set to {@code ValueType.BOOLEAN} after this call completes.
     *
     * @return This value as a boolean.
     */
    public boolean asBoolean() {
        boolean ret;
        if (type == ValueType.BOOLEAN) {
            ret = (boolean) value;
        } else if (type == ValueType.INT) {
            ret = (int) value != 0;
        } else if (type == ValueType.DOUBLE) {
            ret = (double) value != 0;
        } else if (type == ValueType.OBJECT) {
            ret = ((JsonObject) value).numFields() > 0;
        } else if (type == ValueType.ARRAY) {
            ret = ((List) value).size() > 0;
        } else if (type == ValueType.STRING) {
            ret = value.toString().length() > 0;
        } else {
            // NULL case.
            ret = false;
        }
        type = ValueType.BOOLEAN;
        value = ret;
        return ret;
    }

    /**
     * Gets this value as null. This type is set to {@code ValueType.NULL}, and the value is set to null. This method is
     * not useful, but it is included for completeness.
     *
     * @return null
     */
    public Object asNull() {
        value = null;
        type = ValueType.NULL;
        return null;
    }

    /**
     * Sets and gets this value as undefined.
     * @return
     */
    public ValueType asUndefined() {
        value = ValueType.UNDEFINED;
        type = ValueType.UNDEFINED;
        return ValueType.UNDEFINED;
    }

    /**
     * Returns the String representation of this object. This method returns this value as a JSON parseable String.
     *
     * @return The String representation of this object.
     */
    @Override
    public String toString() {
        if (type == ValueType.STRING) {
            String s = (String) value;
            s = s.replaceAll("\"", "\\\\\"");
            return "\"" + s + "\"";
        } else if (type == ValueType.ARRAY) {
            LinkedList list = values();
            StringBuilder b = new StringBuilder().append('[');
            for (Object o : list) {
                JsonValue v = (JsonValue) o;
                if (v.getType() != ValueType.UNDEFINED) {
                    b.append(o.toString()).append(',');
                }
            }
            if (b.charAt(b.length() - 1) == ',') {
                b.deleteCharAt(b.length() - 1);
            }
            return b.append(']').toString();
        } else {
            return String.valueOf(value);
        }
    }

}
