package sej.json;

/**
 *
 * @author Steven Jeffries
 */
public interface JsonCallback {
    public void callback(JsonObject obj);
}
