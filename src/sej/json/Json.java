package sej.json;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;


/**
 * @author Steven Jeffries
 */
public final class Json {

    /**
     * The string trying to be parsed.
     */
    private String json;

    /**
     * The characters from the json string.
     */
    private char[] chars;

    /**
     * The current position in the characters array.
     */
    private int pos;

    /**
     * The current line number.
     */
    private int line;

    /**
     * The current column within the line.
     */
    private int col;

    /**
     * The singleton instance.
     */
    private static Json instance = null;

    private Json() {
    }

    /**
     * Gets the instance of this singleton.
     *
     * @return The instance
     */
    public static Json instance() {
        if (instance == null) {
            instance = new Json();
        }
        return instance;
    }

    /**
     * Gets the next char in the json string.
     *
     * @return The next char (0 if end of string).
     * @throws JsonParseException if an error occurs.
     */
    private char nextChar() throws JsonParseException {
        return nextChar(false);
    }

    /**
     * Gets the next char in the json string. If isString is true,
     * special consideration is taken to make sure there isn't a new
     * line character in the middle of the string, and to include whitespace.
     *
     * @param isString True if this is the next char in a string.
     * @return the next character, or 0 if end of the string is reached.
     * @throws JsonParseException
     */
    private char nextChar(boolean isString) throws JsonParseException {
        pos++;
        if (isString) {
            col++;
            if (pos >= chars.length) {
                return 0;
            } else {
                if (chars[pos] == '\n') {
                    throw new JsonParseException("Expecting string value, but newline character found.", json, line, col);
                }
                return chars[pos];
            }
        }
        for (; pos < chars.length; pos++) {
            col++;
            if (!Character.isWhitespace(chars[pos])) {
                return chars[pos];
            } else if (chars[pos] == '\n') {
                line++;
                col = 0;
            }
        }
        return 0;
    }

    /**
     * Parses and returns a JsonValue representing an object.
     *
     * @return The parsed JsonValue representing an object.
     * @throws JsonParseException if an error occurs.
     */
    private JsonValue parseObject() throws JsonParseException {
        JsonObject obj = new JsonObject();

        char c;

        String key = null;
        boolean seperatorFound = false;
        while ((c = nextChar()) != 0) {
            if (c == '/') {
                skipComment();
            } else if (c == '}') {
                return new JsonValue(obj);
            } else if (key == null) {
                if (c != '"' && c != '\'') {
                    throw new JsonParseException("Expecting string key, but found character '" + c + "'.", json, line, col);
                } else {
                    key = nextString();
                }
            } else if (!seperatorFound) {
                if (c != ':') {
                    throw new JsonParseException("Expecting key separator ':' but found character '" + c + "'.", json, line, col);
                } else {
                    seperatorFound = true;
                }
            } else {
                // Bleh.
                col--;
                obj.set(key, parsePart());
                key = null;
                seperatorFound = false;
                c = nextChar();
                if (c == '}') {
                    return new JsonValue(obj);
                } else if (c == 0) {
                    throw new JsonParseException("Expecting comma or end of object, but end of string reached.", json, line, col);
                } else if (c != ',') {
                    throw new JsonParseException("Expecting comma or end of object, but found '" + c + "'.", json, line, col);
                }
            }
        }
        throw new JsonParseException("Expecting to build object, but end of string found.", json, line, col);
    }

    /**
     * Pulls the next string separated by single or double quotes out of the json string.
     *
     * @return the next string (may or may not be an empty string, but is never null)
     * @throws JsonParseException if an error occurs.
     */
    private String nextString() throws JsonParseException {
        char opener = chars[pos];
        int start = pos + 1;
        char c = nextChar(true);
        // Avoids skipping the character after an escape character if the escape character itself is escaped.
        // Example: \n will skip \ and n, \\n will skip \ and \
        boolean lastEscape = false;
        while (c != opener && c != 0) {
            if (c == '\\' && !lastEscape) {
                c = nextChar(true);
                lastEscape = true;
                continue;
            }
            lastEscape = false;
            c = nextChar(true);
        }
        if (c == opener) {
            return new String(chars, start, pos - start);
        } else {
            throw new JsonParseException("Reached end of string, but was expecting '" + opener + "'.", json, line, col);
        }
    }

    /**
     * Parses an array segment of a json string and returns it as a JsonValue.
     *
     * @return the parsed array
     * @throws JsonParseException if an error occurs.
     */
    private JsonValue parseArray() throws JsonParseException {
        if (chars[pos] != '[') {
            throw new JsonParseException("Expecting array start but got '" + chars[pos] + "' instead.", json, line, col);
        }
        char c;
        LinkedList<JsonValue> values = new LinkedList<>();
        while ((c = nextChar()) != 0 && c != ']') {
            values.add(parsePart());
            c = nextChar();
            if (c == ']') {
                break;
            } else if (c == 0) {
                throw new JsonParseException("Expecting comma or end of array, but end of string reached.", json, line, col);
            } else if (c != ',') {
                throw new JsonParseException("Expecting comma or end of array, but got '" + c + "'.", json, line, col);
            }
        }
        if (c == ']') {
            return new JsonValue(values);
        } else {
            throw new JsonParseException("Expecting comma or end of array, but end of string reached.", json, line, col);
        }
    }

    /**
     * Gets the next string as a JsonValue
     *
     * @return the next string
     * @throws JsonParseException if an error occurs.
     */
    private JsonValue parseString() throws JsonParseException {
        return new JsonValue(nextString());
    }

    /**
     * Parses a number from the json string.
     *
     * @return the parsed number as a JsonValue.
     * @throws JsonParseException if an error occurs.
     */
    private JsonValue parseNumber() throws JsonParseException {
        boolean dotFound = false;
        boolean eFound = false;
        boolean numFound = false;
        if (chars[pos] == '.') {
            dotFound = true;
        } else if (chars[pos] >= '0' && chars[pos] <= '9') {
            numFound = true;
        }
        int start = pos;
        pos++;
        for (; pos < chars.length; pos++) {
            col++;
            if (Character.isWhitespace(chars[pos])) {
                col--;
                pos--;
                break;
            } else {
                if (chars[pos] == '.') {
                    if (dotFound) {
                        throw new JsonParseException("Unexpected '.'.", json, line, col);
                    } else {
                        dotFound = true;
                    }
                } else if (chars[pos] == 'e' || chars[pos] == 'E') {
                    if (eFound) {
                        throw new JsonParseException("Unexpected '" + chars[pos] + "'.", json, line, col);
                    } else {
                        eFound = true;
                    }
                } else if (chars[pos] >= '0' && chars[pos] <= '9') {
                    numFound = true;
                } else if (!numFound) {
                    throw new JsonParseException("Expected value, but found '" + chars[pos] + "' instead.", json, line, col);
                } else {
                    col--;
                    pos--;
                    break;
                }
            }
        }
        // Math.min in case a number is the only thing parsed. e.g. Json.parse("4");
        String num = new String(chars, start, Math.min(pos - start + 1, chars.length));
        if (dotFound) {
            try {
                return new JsonValue(Double.parseDouble(num));
            } catch (Exception ex) {
                throw new JsonParseException("Unable to parse number.", json, line, start);
            }
        } else if (eFound) {
            try {
                return new JsonValue(Double.valueOf(num).intValue());
            } catch (Exception ex) {
                throw new JsonParseException("Unable to parse number.", json, line, start);
            }
        } else {
            try {
                return new JsonValue(Integer.parseInt(num));
            } catch (Exception ex) {
                throw new JsonParseException("Unable to parse number.", json, line, start);
            }
        }
    }

    /**
     * Once an 'n' is reached, it's assumed that null is the value being parsed.
     * This just ensures that the value is actually null.
     *
     * @return null as a JsonValue.
     * @throws JsonParseException if an error occurs.
     */
    private JsonValue parseNull() throws JsonParseException {
        if (pos + 3 >= chars.length) {
            throw new JsonParseException("Expecting value, but end of string reached.", json, line, col);
        }
        if (chars[pos] == 'n' && chars[pos + 1] == 'u' && chars[pos + 2] == 'l' && chars[pos + 3] == 'l') {
            col += 3;
            pos += 3;
            return new JsonValue((Object) null);
        } else {
            throw new JsonParseException("Expected null or value, but got 'n' instead.", json, line, col);
        }
    }

    /**
     * Skips a comment.
     *
     * @throws JsonParseException if an error occurs.
     */
    private void skipComment() throws JsonParseException {
        if (pos + 3 >= chars.length) {
            throw new JsonParseException("Expecting value, but end of string reached.", json, line, col);
        }
        if (chars[pos + 1] != '*') {
            throw new JsonParseException("Expecting value, but found '/'.", json, line, col);
        }
        pos++;
        col++;
        for (; pos + 1 < chars.length; pos++) {
            col++;
            if (Character.isWhitespace(chars[pos])) {
                if (chars[pos] == '\n') {
                    line++;
                    col = 0;
                }
            }
            if (chars[pos] == '*' && chars[pos + 1] == '/') {
                col++;
                pos++;
                return;
            }
        }
        throw new JsonParseException("Expected end of comment, but end of string reached.", json, line, col);
    }

    /**
     * Parses a part of a json string and returns a JsonValue
     *
     * @return a JsonValue representing the part of the json string.
     * @throws JsonParseException if an error occurs reading the string.
     */
    private JsonValue parsePart() throws JsonParseException {
        for (; pos < chars.length; pos++) {
            char c = chars[pos];
            col++;
            JsonValue part = null;
            if (!Character.isWhitespace(c)) {
                if (c == '{') {
                    part = parseObject();
                } else if (c == '[') {
                    part = parseArray();
                } else if (c == '"' || c == '\'') {
                    part = parseString();
                } else if ((c >= '0' && c <= '9') || (c == '.') || (c == '-')) {
                    part = parseNumber();
                } else if (c == 'n') {
                    part = parseNull();
                } else if (c == '/') {
                    skipComment();
                    continue;
                } else {
                    throw new JsonParseException("Unexpected character '" + c + "'.", json, line, col);
                }
                if (part == null) {
                    throw new JsonParseException("Unexpected error occurred.", json, line, col);
                } else {
                    return part;
                }
            } else if (c == '\n') {
                line++;
                col = 0;
            }
        }
        throw new JsonParseException("Reached end of string, but expected value.", json, line, col);
    }

    /**
     * Parses a json string and returns a JsonValue.
     *
     * @param str The string to parse.
     * @return The JsonValue representing the parsed string.
     * @throws JsonParseException if an error occurs while parsing.
     */
    public JsonValue parseJson(String str) throws JsonParseException {
        json = str;
        chars = str.toCharArray();
        line = 1;
        col = 0;
        pos = 0;
        return parsePart();
    }

    /**
     * Parses a json string and returns a JsonValue.
     *
     * @param str The string to parse.
     * @return The JsonValue representing the parsed string.
     * @throws JsonParseException if an error occurs while parsing.
     */
    public static JsonValue parse(String str) throws JsonParseException {
        return instance().parseJson(str);
    }

    /**
     * Loads JSON from a file.
     *
     * @param file The file to load.
     * @return The constructed JsonValue.
     * @throws IOException        If there is a read/write error.
     * @throws JsonParseException If there is an error parsing the JSON.
     */
    public static JsonValue load(File file) throws IOException, JsonParseException {
        return load(new FileInputStream(file));
    }

    /**
     * Loads JSON from a file given by path.
     *
     * @param path The path to load the JSON from.
     * @return The constructed JsonValue.
     * @throws IOException        If there is a read/write error.
     * @throws JsonParseException If there is an error parsing the JSON.
     */
    public static JsonValue load(String path) throws IOException, JsonParseException {
        return load(new File(path));
    }

    /**
     * Loads JSON from an input stream.
     *
     * @param in The input stream to load the JSON from.
     * @return The constructed JsonValue.
     * @throws IOException        If there is a read/write error.
     * @throws JsonParseException If there is an error parsing the JSON.
     */
    public static JsonValue load(InputStream in) throws IOException, JsonParseException {
        return load(in, null);
    }

    /**
     * Loads JSON from a file with a specific encoding.
     *
     * @param file     The file to load the JSON from.
     * @param encoding The encoding to load.
     * @return The constructed JsonValue.
     * @throws IOException        If there is a read/write error.
     * @throws JsonParseException If there is an error parsing the JSON.
     */
    public static JsonValue load(File file, String encoding) throws IOException, JsonParseException {
        return load(new FileInputStream(file), encoding);
    }

    /**
     * Loads JSON from a file given by path with a specific encoding.
     *
     * @param path     The path to load the JSON from.
     * @param encoding The encoding to load.
     * @return The constructed JsonValue.
     * @throws IOException        If there is a read/write error.
     * @throws JsonParseException If there is an error parsing the JSON.
     */
    public static JsonValue load(String path, String encoding) throws IOException, JsonParseException {
        return load(new File(path), encoding);
    }

    /**
     * Loads JSON from an input stream with a specific encoding.
     *
     * @param in       The input stream to load the JSON from.
     * @param encoding The encoding to load.
     * @return The constructed JsonValue.
     * @throws IOException        If there is a read/write error.
     * @throws JsonParseException If there is an error parsing the JSON.
     */
    public static JsonValue load(InputStream in, String encoding) throws IOException, JsonParseException {
        byte[] buf = new byte[1024];
        int length;
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        while ((length = in.read(buf)) != -1) {
            os.write(buf, 0, length);
        }
        String text;
        if (encoding != null) {
            text = new String(os.toByteArray(), encoding);
        } else {
            text = new String(os.toByteArray());
        }
        return parse(text);
    }

}
