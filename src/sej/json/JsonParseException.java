package sej.json;

/**
 *
 * @author Steven Jeffries
 */
public class JsonParseException extends Exception {

    private int line, col;
    private String json;


    public int getLine() {
        return line;
    }

    public int getCol() {
        return col;
    }

    public String getJson() {
        return json;
    }

    public JsonParseException(String message, String json, int lineNum, int col) {
        super(JsonParseException.getMessage(message, json, lineNum, col));
        this.line = lineNum;
        this.col = col;
        this.json = json;
    }

    private static String getMessage(String message, String json, int lineNum, int col) {
        String[] lines = json.split("\n");
        String line = lines[lineNum - 1];
        int arrowIndex = col;
        if (line.length() > 40) {
            int middle = col;
            if (middle + 20 > line.length()) {
                middle = line.length() - 20;
            } else if (middle - 20 < 0) {
                middle = 20;
            }
            boolean prepend = false;
            if (middle - 20 > 0) {
                prepend = true;
            }
            boolean append = false;
            if (middle + 20 < line.length()) {
                append = true;
            }
            line = line.substring(middle - 20, middle + 20);
            line = "(" + line + ")";
            if (append) {
                line += "...";
            }
            if (prepend) {
                line = "..." + line;
                arrowIndex += 3;
            }
            arrowIndex -= middle - 20;
        } else {
            line = "(" + line + ")";
        }
        String lineNumStr = "[" + lineNum + ":" + col + "] ";
        String arrowTop;
        arrowTop = "⥣";
        for (int i = 0; i < arrowIndex + lineNumStr.length(); i++) {
            arrowTop = " " + arrowTop;
        }
        message += "\n\n" + lineNumStr + line + "\n" + arrowTop + "\n";
        return message;
    }
}
