package sej.json;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Steven Jeffries
 */
public class JsonObject {

    public static interface JsonLooper {

        public void each(String key, JsonValue val);
    }

    public static interface JsonFilter {

        public boolean filter(String key, JsonValue val);
    }

    private final HashMap<String, JsonValue> values;

    public JsonObject() {
        values = new HashMap<>();
    }

    /**
     * Creates a deep copy of this object.
     *
     * @return A deep copy of this object.
     */
    public JsonObject copy() {
        try {
            JsonValue jv = Json.parse(toString());
            JsonObject obj = jv.asObject();
            return Json.parse(toString()).asObject();
        } catch (JsonParseException ex) {
            // Should never happen when parsing the string made from this object.
            return null;
        }
    }

    /**
     * Merges this object with another one. The values in this object will stay set, but any fields declared in the
     * other object that are not declared in this one will be added.
     *
     * @param other The other object to merge with.
     * @return A reference back to this object for chaining.
     */
    public JsonObject mergeWith(JsonObject other) {
        other = other.copy();
        for (String var : other.values.keySet()) {
            if (!values.containsKey(var)) {
                values.put(var, other.values.get(var));
            }
        }
        return this;
    }

    /**
     * Gets a mapping of the fields of this object.
     *
     * @return A mapping of field names and field values of this object.
     */
    public Map<String, JsonValue> getFields() {
        return new HashMap<>(values);
    }

    /**
     * Gets a mapping of the fields of this object.
     * <p>
     * Example usage: {@code Map<String, JsonValue> arrays = obj.filter((k, v) -> v.getType() == ValueType.ARRAY);}
     *
     * @param filter Return true to get the item, false otherwise.
     * @return A filtered mapping of field names and field values of this object.
     */
    public Map<String, JsonValue> getFields(JsonFilter filter) {
        HashMap<String, JsonValue> ret = new HashMap<>();
        for (String var : values.keySet()) {
            if (filter.filter(var, values.get(var))) {
                ret.put(var, values.get(var));
            }
        }
        return ret;
    }

    /**
     * Gets the field by name, or creates a new one if that field does not exist.
     *
     * @param var The name of the field.
     * @return The JsonValue of the field, or null if the field is undefined.
     */
    public JsonValue get(String var) {
        if (!values.containsKey(var)) {
            JsonValue val = JsonValue.undefined();
            values.put(var, val);
            val.owner = this;
            return val;
        } else {
            JsonValue val = values.get(var);
            val.owner = this;
            return val;
        }
    }

    public boolean getBoolean(String var) {
        return get(var).asBoolean();
    }

    public boolean getBoolean(String var, boolean def) {
        if (!values.containsKey(var)) {
            return def;
        }
        if (values.get(var).getType() != ValueType.BOOLEAN) {
            return def;
        }
        return values.get(var).asBoolean();
    }

    public int getInt(String var) {
        return get(var).asInt();
    }

    public int getInt(String var, int def) {
        if (!values.containsKey(var)) {
            return def;
        }
        if (values.get(var).getType() != ValueType.INT) {
            return def;
        }
        return values.get(var).asInt();
    }

    public double getDouble(String var) {
        return get(var).asDouble();
    }

    public double getDouble(String var, double def) {
        if (!values.containsKey(var)) {
            return def;
        }
        if (values.get(var).getType() != ValueType.DOUBLE) {
            return def;
        }
        return values.get(var).asDouble();
    }

    public String getString(String var) {
        return get(var).asString();
    }

    public String getString(String var, String def) {
        if (!values.containsKey(var)) {
            return def;
        }
        if (values.get(var).getType() != ValueType.STRING) {
            return def;
        }
        return values.get(var).asString();
    }

    public JsonObject getObject(String var) {
        return get(var).asObject();
    }

    public JsonObject getObject(String var, JsonObject def) {
        if (!values.containsKey(var)) {
            return def;
        }
        if (values.get(var).getType() != ValueType.OBJECT) {
            return def;
        }
        return values.get(var).asObject();
    }

    public List<JsonValue> getArray(String var) {
        return get(var).asList();
    }

    public List<JsonValue> getArray(String var, List<JsonValue> def) {
        if (!values.containsKey(var)) {
            return def;
        }
        if (values.get(var).getType() != ValueType.ARRAY) {
            return def;
        }
        return values.get(var).asList();
    }

    public Object getNull(String var) {
        return get(var).asNull();
    }

    /**
     * Completely ignores input Object and returns null. Included for completeness.
     *
     * @param var
     * @param nul
     * @return
     */
    public Object getNull(String var, Object nul) {
        return get(var).asNull();
    }

    public boolean hasField(String field) {
        return values.containsKey(field) && values.get(field).getType() != ValueType.UNDEFINED;
    }

    public boolean hasFields(String... fields) {
        for (String s : fields) {
            if (!hasField(s)) {
                return false;
            }
        }
        return true;
    }

    public void removeField(String... fields) {
        List<String> ex = Arrays.asList(fields);
        Set<String> set = values.keySet();
        set.removeAll(ex);
    }

    public void removeAllFieldsExcept(String... fields) {
        List<String> ex = Arrays.asList(fields);
        Set<String> set = values.keySet();
        set.retainAll(ex);
    }

    public JsonObject set(String var, Object... values) {
        return get(var).set(values);
    }

    public JsonObject setInt(String var, JsonValue.IntMod mod) {
        return get(var).setInt(mod);
    }

    public JsonObject setInt(String var, int val) {
        return get(var).setInt(val);
    }

    public JsonObject setDouble(String var, JsonValue.DoubleMod mod) {
        return get(var).setDouble(mod);
    }

    public JsonObject setDouble(String var, double val) {
        return get(var).setDouble(val);
    }

    public JsonObject setBoolean(String var, JsonValue.BooleanMod mod) {
        return get(var).setBoolean(mod);
    }

    public JsonObject setBoolean(String var, boolean val) {
        return get(var).setBoolean(val);
    }

    public JsonObject setString(String var, JsonValue.StringMod mod) {
        return get(var).setString(mod);
    }

    public JsonObject setString(String var, String val) {
        return get(var).setString(val);
    }

    public JsonObject setObject(String var, JsonValue.ObjectMod mod) {
        return get(var).setObject(mod);
    }

    public JsonObject setObject(String var, JsonObject obj) {
        return get(var).setObject(obj);
    }

    public JsonObject setArray(String var, JsonValue.ArrayMod mod) {
        return get(var).setArray(mod);
    }

    public JsonObject setArray(String var, JsonValue[] arr) {
        return get(var).setArray(arr);
    }

    public JsonObject setArray(String var, JsonValue.ListMod mod) {
        return get(var).setArray(mod);
    }

    public JsonObject setArray(String var, List<JsonValue> vals) {
        return get(var).setArray(vals);
    }

    public JsonObject setArray(String var, Object... values) {
        get(var).setArray((List<JsonValue> vals) -> {
            for (Object o : values) {
                vals.add(new JsonValue(o));
            }
        });
        return this;
    }

    public JsonObject setNull(String var) {
        return get(var).setNull();
    }

    public void each(JsonLooper loop) {
        for (String s : values.keySet()) {
            loop.each(s, values.get(s));
        }
    }

    public int numFields() {
        return values.size();
    }

    public ValueType getType(String var) {
        if (!values.containsKey(var)) {
            return ValueType.UNDEFINED;
        } else {
            return values.get(var).getType();
        }
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder().append('{');
        for (String s : values.keySet()) {
            JsonValue v = values.get(s);
            if (v.getType() != ValueType.UNDEFINED) {
                build.append('"').append(s.replaceAll("\"", "\\\\\"")).append('"').append(':');
                build.append(v.toString()).append(',');
            }
        }
        if (build.indexOf(",") != -1) {
            build.deleteCharAt(build.length() - 1);
        }
        return build.append('}').toString();
    }

}
